import sys
from contextlib import contextmanager
from io import StringIO


class SimpleLogger:
    def __init__(self, stream):
        self.stream = stream
        self.log_string = StringIO()

    def write(self, message):
        self.log_string.write(message)

    def getvalue(self):
        return self.log_string.getvalue()


@contextmanager
def temporary_logger(stream):
    original_stream = getattr(sys, stream)
    logger = SimpleLogger(original_stream)
    setattr(sys, stream, logger)
    try:
        yield logger
    finally:
        setattr(sys, stream, original_stream)