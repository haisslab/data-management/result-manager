from flask import (
    Flask,
    render_template,
    send_from_directory,
    request,
    redirect,
    url_for,
    make_response,
)

import dash
from dash import dcc, html, dash_table
import plotly.graph_objects as go
from dash.dependencies import Input, Output

import one

import os, re, platform
from itertools import groupby
from functools import wraps
from natsort import natsorted
from Inflow.path.one_alf import auto_rename_session_files_to_alf, register_files
import pandas as pd


from utils import temporary_logger


connector = one.ONE()
connector.set_data_access_mode("remote")

server = Flask(__name__)
server.config.suppress_callback_exceptions = True
app = dash.Dash(__name__, server=server, url_base_pathname="/dashboard/")

pages = {
    "Figures images": "/images/{session_uuid}/",
    "Trials Plot": "/dashboard/trials-df/{session_uuid}/plot/",
    "Trials Table": "/dashboard/trials-df/{session_uuid}/",
    "Data Management Plot": "/dashboard/data-management/{session_uuid}/plot/",
    "Data Management Table": "/dashboard/data-management/{session_uuid}/",
    "Data Registration": "/dashboard/data-registration/{session_uuid}/check/",
}

app.layout = html.Div(
    [
        html.Div(className="ribbon", id="ribbon_container", style={"margin-bottom": 75}),
        html.Div(
            children=[
                dcc.Location(id="url", refresh=False),
                dcc.Loading(
                    id="loading",
                    children=[html.Div(id="dash-container")],
                    type="circle",
                    style={"margin-top": 150},
                ),
            ]
        ),
    ]
)


def create_menu(session_uuid, session_alias):
    if session_uuid is None:
        alyx_label = "Go to Alyx"
        alyx_adress = "http://haiss-alyx.local/admin/actions/session/"
        dropdown = []
    else:
        alyx_label = "Open in Alyx"
        alyx_adress = f"http://haiss-alyx.local/admin/actions/session/{session_uuid}/change/"
        dropdown = [
            html.Div(
                [
                    f"Session {session_alias}",
                    html.Div(
                        [html.A(name, href=link.format(session_uuid=session_uuid)) for name, link in pages.items()],
                        className="dropdown-content",
                    ),
                ],
                className="dropdown-link-menu",
                id="session-name-title",
            )
        ]
    print("Session id is : ", session_uuid, "alias is : ", session_alias)
    return (
        [
            html.A(
                "Result Manager",
                href=r"/",
                target="_self",  # force full page reload
                id="home-link",
                role="button",
            ),
            html.A(alyx_label, id="alyx-link", href=alyx_adress),
        ]
        + dropdown
        + [html.I("Dashboard", id="dashboard-title", style={"text-align": "left"})]
    )


@app.callback(Output("ribbon_container", "children"), Input("url", "pathname"))
def update_menu(pathname):
    uuid = extract_uuid_key(pathname)
    if uuid is None:
        alias = ""
    else:
        alias = get_alias(uuid)
    return create_menu(uuid, alias)  # Create menu with updated links


func_routes = {}
kwargs_names_pattern = re.compile(r"(?:<(.*?)>)")


def dash_route(route, strict_slashes=True):
    def wrapper(func):
        global func_routes, kwargs_names_pattern
        kwargs_names = kwargs_names_pattern.findall(route)

        route_pattern = route
        for kwarg in kwargs_names:
            route_pattern = re.sub(f"<{kwarg}>", f"(?P<{kwarg}>[^/]+?)", route_pattern)

        if not strict_slashes:
            route_pattern = route_pattern.rstrip("/") + r"/?"
        route_pattern = re.compile("^" + route_pattern + "$")

        func_routes[route] = {
            "func": func,
            "kwargs_names": kwargs_names,
            "route_pattern": route_pattern,
        }

        @wraps(func)
        def wrapped_func(*args, **kwargs):
            return func(*args, **kwargs)

        return wrapped_func

    return wrapper


@app.callback(Output("dash-container", "children"), [Input("url", "pathname")])
def dash_container_router(pathname):
    if not pathname:
        return dash.no_update
    for route, route_item in func_routes.items():
        if route_item["route_pattern"].match(pathname):
            print(pathname + " match : " + route + "with pattern : " + route_item["route_pattern"].pattern)
            kwargs = {}
            kwags_matches = route_item["route_pattern"].findall(pathname)
            if kwags_matches:
                if len(route_item["kwargs_names"]) > 1:
                    kwags_matches = kwags_matches[0]
                kwargs = {name: match for name, match in zip(route_item["kwargs_names"], kwags_matches)}
            return route_item["func"](**kwargs)  # call the appropriate routed function here, with **kwargs
    return html.Div("404 Not Found: The resource could not be found.", className="error_message")


def perform_data_registration(uuid, dry_run=True):
    uuid = connector.to_eid(uuid)
    session_details = connector.alyx.rest("sessions", "read", id=uuid)
    selected_session_folder = session_details["path"]
    selected_session_folder = convert_mount(selected_session_folder)

    results = auto_rename_session_files_to_alf(selected_session_folder, dry_run=dry_run, return_tables=True)
    results["names_after_renaming"] = [convert_mount(path, reverse=True) for path in results["names_after_renaming"]]
    try:
        register_files(results["names_after_renaming"], uuid, dry_run=dry_run)
    except:
        pass
    return results


@dash_route("/dashboard/data-registration/<uuid>/check", strict_slashes=False)
def display_data_registration_check(uuid):
    with temporary_logger("stdout") as logger:
        tables = perform_data_registration(uuid, dry_run=True)
        log = logger.getvalue()
    log_text = html.Pre(log, style={"whiteSpace": "pre-wrap", "wordBreak": "break-all"})
    conflict_table = display_table(tables["conflicts_df"], "Conflicts Table")
    comparison_table = display_table(tables["comparison_df"], "File Renaming Comparisons Table")

    redirect_button = html.Div(
        dcc.Link(
            [
                "I confirm having carefully checked the output :",
                html.Br(),
                html.B("Perform Registration Now", style={"font-weight": "bold"}),
            ],
            href="/dashboard/data-registration/<uuid>/perform_after_check",
            className="perform_registration_button",
        ),
        className="button_container",
    )

    return html.Div(
        [
            conflict_table,
            comparison_table,
            log_text,
            redirect_button,
        ]
    )


def display_table(df, title=None):
    def drop_multiliners(cell):
        try:
            len(cell.iloc[0])
            if isinstance(cell.iloc[0], str):
                return False
            return True
        except (TypeError, AttributeError):
            return False

    if not isinstance(df, pd.core.frame.DataFrame):
        return df  # probably is an html error object, return so we can show it

    sel = df.apply(drop_multiliners)
    multi_columns = sel[sel == True].index
    drop_columns = []
    for column in multi_columns:
        lenghts = df[column].apply(lambda cell: len(cell))
        if lenghts.max() == 1 and lenghts.min() == 1:
            df[column] = df[column].apply(lambda cell: cell[0])
        else:
            drop_columns.append(column)
    df = df.drop(columns=drop_columns)

    # Limit the number of rows sent to frontend
    # df = df.head(50)

    int_columns = df.select_dtypes(include="int").columns.tolist()
    df[int_columns] = df[int_columns].astype(str)

    if title is None:
        title = ""

    style_header = {"font-family": "Roboto, sans-serif", "font-weight": "bold", "font-size": "16px"}
    style_table = {"width": "100vw", "overflow-x": "auto"}
    style_cell = {"font-family": "Roboto, sans-serif", "font-size": "14px", "text-align": "center"}
    if len(df):
        style_cell.update({"width": "{}%".format(100 / len(df.columns))})

    return html.Div(
        [
            html.Div(title, className="table_title"),
            dash_table.DataTable(
                data=df.to_dict("records"),
                columns=[{"name": i, "id": i} for i in df.columns],
                filter_action="native",
                sort_action="native",
                sort_mode="multi",
                page_action="native",
                page_size=25,
                style_header=style_header,
                style_table=style_table,
                style_cell=style_cell,
                id="pandas_table",
            ),
        ]
    )


def display_2d_plot(df):
    if not isinstance(df, pd.core.frame.DataFrame):
        return df  # probably is an html error object, return so we can show it

    jsonified_df = df.to_json(date_format="iso")

    return html.Div(
        [
            html.Div(
                [
                    html.Div(
                        [
                            html.Label(
                                "X Variable",
                                style={
                                    "display": "inline-block",
                                    "verticalAlign": "middle",
                                },
                            ),
                            dcc.Dropdown(
                                id="x-variable",
                                options=[{"label": i, "value": i} for i in df.columns],
                                value=df.columns[0],
                                style={
                                    "display": "inline-block",
                                    "verticalAlign": "middle",
                                    "width": "400px",
                                    "marginLeft": "10px",
                                },
                            ),
                        ],
                        style={"textAlign": "center"},
                    ),
                    html.Div(
                        [
                            html.Label("Y Variable"),
                            dcc.Dropdown(
                                id="y-variable",
                                options=[{"label": i, "value": i} for i in df.columns],
                                value=df.columns[1],
                                style={
                                    "display": "inline-block",
                                    "verticalAlign": "middle",
                                    "width": "400px",
                                    "marginLeft": "10px",
                                },
                            ),
                        ],
                        style={"textAlign": "center"},
                    ),
                ],
                style={
                    "border": "1px solid #d3d3d3",
                    "borderRadius": "5px",
                    "padding": "20px",
                    "background": "#f9f9f9",
                    "maxWidth": "500px",
                    "margin": "auto",
                },
            ),
            html.Div(
                dcc.Graph(
                    id="2d-graph",
                    style={
                        "width": "70vw",
                        "height": "50vh",
                        "display": "inline-block",
                    },
                ),
                style={"textAlign": "center"},
            ),
            html.Div(jsonified_df, id="jsonified_df", style={"display": "none"}),
            # just a placeholder to avoid recomputing the dataframe when selecting different cross sections
        ],
    )


@app.callback(
    Output("2d-graph", "figure"),
    [
        Input("x-variable", "value"),
        Input("y-variable", "value"),
        Input("jsonified_df", "children"),
    ],
)
def update_2d_graph(x, y, jsonified_df):
    df = pd.read_json(jsonified_df)

    fig = go.Figure(go.Scatter(x=df[x], y=df[y], mode="markers"))
    fig.update_layout(
        autosize=True,
        margin=dict(l=50, r=50, b=100, t=100, pad=4),
        xaxis=dict(title=x),
        yaxis=dict(title=y),
    )
    return fig


def get_trials_df(uuid):
    uuid = connector.to_eid(uuid)
    if uuid is None:
        return html.Div(
            "404 Not Found: The supplied session doesn't exist",
            className="error_message",
        )
    session = connector.to_session_details(connector.alyx.rest("sessions", "read", id=uuid))

    df_path = os.path.join(
        convert_mount(session.path),
        "preprocessing_saves",
        "adaptation.trials_df.pickle",
    )
    df = pd.read_pickle(df_path)
    df.loc[:, "trial_nb"] = df.index
    df = df.set_index("trial_nb").reset_index()
    return df


@dash_route("/dashboard/trials-df/<uuid>/", strict_slashes=False)
def display_trials_df_table(uuid):
    try:
        return display_table(get_trials_df(uuid), "Trials Table")
    except FileNotFoundError:
        return html.Div(
            "404 Not Found: adaptation.trials_df was not found for this session",
            className="error_message",
        )


@dash_route("/dashboard/trials-df/<uuid>/plot/", strict_slashes=False)
def display_trials_df_plot(uuid):
    try:
        return display_2d_plot(get_trials_df(uuid))
    except FileNotFoundError:
        return html.Div(
            "404 Not Found: adaptation.trials_df was not found for this session",
            className="error_message",
        )


def get_data_management_df(uuid):
    uuid = connector.to_eid(uuid)
    if uuid is None:
        return html.Div(
            "404 Not Found: The supplied session doesn't exist",
            className="error_message",
        )
    return connector.list_datasets(uuid, details=True)


@dash_route("/dashboard/data-management/<uuid>/", strict_slashes=False)
def display_data_management_table(uuid):
    return display_table(get_data_management_df(uuid), "Registered Data Table")


@dash_route("/dashboard/data-management/<uuid>/plot/", strict_slashes=False)
def display_data_management_plot(uuid):
    return display_2d_plot(get_data_management_df(uuid))


@dash_route("/dashboard/")
def dashboard_home():
    return dcc.Location(pathname="/", id="url_redirecting", refresh=True)


@server.route("/images/<uuid_key>", strict_slashes=False)
def serve_images(uuid_key):
    def render():
        nonlocal uuid_key, session_object, images
        return render_template(
            "images_gallery.html",
            image_groups=images,
            dropdown_links={key: link.format(session_uuid=uuid_key) for key, link in pages.items()},
            session_alias=session_object.alias,
            session_uuid=uuid_key,
        )

    images = [[]]
    uuid_key = connector.to_eid(uuid_key)  # uuid key is provided by the page url
    if uuid_key is None:
        return "Error: Invalid UUID key provided", 400
    session_object = connector.to_session_details(connector.alyx.rest("sessions", "read", id=uuid_key, no_cache=True))
    folder_path = os.path.join(session_object.path, "figures")
    try:
        folder_path = convert_mount(folder_path)
    except IOError:
        return render()
    try:
        image_files = [file for file in os.listdir(folder_path) if file.endswith((".png", ".jpg"))]
    except (
        FileNotFoundError
    ) as e:  # FileNotFoundError if toplevel figures listed folder does not exist for this session.
        return render()

    image_files.sort()
    image_groups = [natsorted(list(group)) for _, group in groupby(image_files, lambda x: x.split(".")[1])]
    images = [
        [
            {
                "filename": file,
                "path": os.path.join(folder_path, file),
            }
            for file in group
        ]
        for group in image_groups
    ]
    return render()


def convert_mount(path, reverse=False):
    available_mounts = {
        "//cajal/cajal_data2/ONE": "/mnt/one/cajal2",
        "//Mountcastle/lab/data/ONE": "/mnt/one/mountcastle1",
    }
    if "ubuntu" in platform.version().lower():
        path = path.replace("\\", "/")
        if reverse:
            for original_path, mounted_path in available_mounts.items():
                if mounted_path in path:
                    path = path.replace(mounted_path, original_path)
                    return os.path.normpath(
                        path
                    )  # we found one mount in the root. breaking and skipping the for - else clause
            # None of the mounts were in the root. Maybe data is in an unmounted location ?
            raise IOError("Could not reverse find a mounted location for this path")
        else:
            for original_path, mounted_path in available_mounts.items():
                if original_path in path:
                    path = path.replace(original_path, mounted_path)
                    return os.path.normpath(
                        path
                    )  # we found one mount in the root. breaking and skipping the for - else clause
            # None of the mounts were in the root. Maybe data is in an unmounted location ?
            raise IOError("Could not find a mounted location for this path")
    else:
        return os.path.normpath(path)


@server.route("/image-directory/<path:path>")
def send_image(path):
    folder, filename = os.path.split(path)
    if not (folder[0] == "/" or folder[0] == "\\") and "ubuntu" in platform.version().lower():
        folder = (
            "/" + folder
        )  # we make sure to have / in front of folder path to make sure the path is absolut, if we run on ubuntu
    return send_from_directory(folder, filename)


def get_alias(uuid):
    return connector.alyx.rest("sessions", "read", id=uuid)["alias"]


def extract_uuid_key(string):
    matches = re.findall(r"\w{8}-\w{4}-\w{4}-\w{4}-\w{12}", string)
    if len(matches):
        uuid_key = matches[0]
    else:
        try:
            uuid_key = connector.to_eid(string)
        except ValueError:
            uuid_key = None
    return uuid_key


@server.route("/", methods=["GET", "POST"], strict_slashes=False)
def home():
    error_message = ""
    if request.method == "POST":
        uuid_key = request.form.get("uuid_key").strip(" ")
        uuid_key = extract_uuid_key(uuid_key)
        if uuid_key is None:
            error_message = "Invalid experiment ID provided. Should be either a subject/date/number combo like wm29/2023-05-25/002 or a UUID like c9392479-a203-4ebe-87ff-1fa0c5dbedc1"
            return render_template("index.html", error_message=error_message)
        return redirect(url_for("serve_images", uuid_key=uuid_key))
    return render_template("index.html", error_message=error_message)


if __name__ == "__main__":
    app.run(debug=True)
